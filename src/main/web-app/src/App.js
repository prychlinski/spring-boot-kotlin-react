import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <Hello/>
        </header>
      </div>
  );
}

function Hello() {
  const [hello, setHello] = useState({msg: 'Loading...'})

  const fetchData = async onFetched => {
    const response = await fetch("/hello");
    onFetched(await response.json());
  };

  useEffect(() => {
    fetchData(setHello);
  }, []);

  return (
      <h2>{hello.msg}</h2>
  );
}

export default App;
