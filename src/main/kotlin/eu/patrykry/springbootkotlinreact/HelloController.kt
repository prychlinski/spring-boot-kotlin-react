package eu.patrykry.springbootkotlinreact

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController {
    @GetMapping("/hello")
    fun hello(): Hello = Hello("Ahoj przygodo")
}

data class Hello(val msg: String)