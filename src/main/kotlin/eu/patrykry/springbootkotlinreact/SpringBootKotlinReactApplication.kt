package eu.patrykry.springbootkotlinreact

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootKotlinReactApplication

fun main(args: Array<String>) {
    runApplication<SpringBootKotlinReactApplication>(*args)
}
